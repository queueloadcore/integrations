/**
* FileName: isSlackAuthenticated
* @description :: Policy to verify authenticity of an user via JSON Web Token
*/

module.exports = function(req, res, next) 
{
	if(req.body.token !== sails.config.slackcfg.SLACK_VERIFICATION_TOKEN)
	{
		var error = new Error();
		error.code = 401;
		error.message = "Incorrect verification code";
		next(error);
	}
	else
	{
		next();	
	}  
};