/**
* FileName: isAwsCwAuthenticated.js
* @description :: Policy to verify authenticity of a request from cloudwatch
*/

var errorCode = require("../resources/ErrorCodes.js");
var pgp = require('pg-promise')();
var cache = require('memory-cache');
var enums = require('../resources/Enums.js');

module.exports = function(req, res, next) 
{
  if(!req.headers['content-type'] || (!req.headers['content-type'].includes('application/json') && !req.headers['content-type'].includes('text/plain')) || !req.query.qlaccountid || !req.query.awscwkey)
  {
    var error = new Error();
    error.code = 400051;
    error.message = errorCode['400051'];
    next(error);
  }
  else
  {
    var awscwkey = cache.get(req.query.qlaccountid + '-awscwkey');
    if(awscwkey === req.query.awscwkey)
    {
      next();
    }
    else
    {
      var db = cache.get(req.query.qlaccountid);
      if(!db)
      {
        var connection = 
        {
          database: req.query.qlaccountid,
          user: req.query.qlaccountid,
          password: req.query.qlaccountid
        };
        if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
        {
          connection.host = sails.config.connections.localDataRepo.host;
          connection.port = sails.config.connections.localDataRepo.port;
        }
        else if(sails.config.environment === 'production')
        {
          connection.host = sails.config.connections.awsDataRepo.host;
          connection.port = sails.config.connections.awsDataRepo.port;
        }
        else
        {
          next({message: "No environment set!!"});
        }
        db = pgp(connection);
        cache.put(req.query.qlaccountid, db);
      }
      var getCwKey = db.any(
        {
          text: "select config from integrationconfigs where integrationtype=$1;", 
          values: [enums.integrationService.CLOUDWATCH]
        }
      );
      getCwKey
      .then(function (result)
      {
        if(!result.length || !result[0].config || (result[0].config.awscwkey !== req.query.awscwkey) || (req.body.Type !== "SubscriptionConfirmation" && !result[0].config.confirmed))
        {
          var error = new Error();
          error.code = 401;
          error.message = "Incorrect verification code";
          next(error);
        }
        else
        {
          cache.put(req.query.qlaccountid + '-awscwkey', req.query.awscwkey);
          next();
        }
      })
      .catch(function (error)
      {
        next(error);
      });
    }
  }  
};