/* FileName: ErrorCodes.js
* @description: This file contains all the error codes and respective messages.
*
* Date                        Change Description                               Author
* ---------               ---------------------------                        -----------
* 27/02/2016                Initial file creation                             Smruti Mandal
*
* Error Codes:
*
* Types of standard error codes:
*
* 1. bad Request (400)
* 2. Forbidden (403)
* 3. not found (404)
* 4. Server Error (500)
*
* each type will be further customized as following:
*
* For custom bad request errors it will be 400XXX. Where each module will have its own reservation of 50 codes, except for system related errors which have 100 reserved for them. Hence
*
* 1 – 50    System
* 51 - 100  Misc
* 101 – 150 User module
* 151 – 200 Business Department module
* 201 – 250 Chargeback module
* 251 – 300 Queue module
* 301 – 400 CMDB Module
* 401 – 450 Incident module
* 451 – 500 Change module

The above holds true for all standard error codes

*/

/*
Empty list result for all
Only active/inactive are allowed - for all entities - ("one of the entites")
One or more of the supplied entities do not exist - for all enties
One or more of the entities are already in the expected state - for all entities
New queue owner must be an active user." - Queue
New incident manager must be an active user." - Queue
New change manager must be an active user." - Queue
New service request manager must be an active user." - Queue
One of the users is an owner of an active Business Department - User
One of the users is an owner of an active Chargeback code - User
One of the users is an owner of an active queue - User
One of the users is a manager of an active queue - User
One of the users is the only admin of an active business department." - User
*/

module.exports =
{
  // System Error Codes
  500001:"System Error. Please contact your administrator",

  //Authentication Error Codes - 501 - 550
  401501:"Requested user doesn't exist",
  401502:"Requested user is not active",
  401503:"Provided credentials are incorrect",
  401504:"Invalid JWT",
  401505:"Token is expired",
  401506:"Token doesn't match",
  401507:"User not signed-in",
  401508:"User password expired",

  //Authorization Error Codes
  403501:"The requester is not an admin of the BD",
  403502:"The requester doesn't have the required privilege",

  // Misc Error Codes
  400051:"Incorrect Parameters. Correct and try again",
  400052:"No Valid fields submitted for update.",
  400053:"One or more entered columns in the search parameters do not exist",

  // User error codes 101 - 150
  400101:"No users available in the system",
  400102:"One or more of the entered users are active",
  400103:"One or more of the entered users are inactive",
  400104:"One or more of the entered users do not exist",
  400105:"One or more of the entered users are in the expected state",
  400106:"One of the users is an owner of an active Business Department",
  400107:"One of the users is an owner of an active Chargeback code",
  400108:"One of the users is an owner of an active queue",
  400109:"One of the users is a manager of an active queue",
  400110:"Previous password cannot be used",
  400111:"One or more users have open incidents",

  // Business Department Error Codes 151 - 200
  400151:"No Business Departments available in the system",
  400152:"One or more of the entered Business Departments are active",
  400153:"One or more of the entered Business Departments are inactive",
  400154:"One or more of the entered Business Departments do not exist",
  400155:"One or more of the entered Business Departments are in the expected state",
  400156:"One or more of the entered users is an admin.",
  400157:"One or more of the entered users is not an admin.",
  400158:"An Active Business Department must have atleast one active admin.",
  400159:"One of the Chargeback codes owned by a Business Department is still active.",

  // Chargeback code Error Codes 201 - 250
  400201:"No Chargeback codes available in the system",
  400202:"One or more of the entered Chargeback codes are active",
  400203:"One or more of the entered Chargeback codes are inactive",
  400204:"One or more of the entered Chargeback codes do not exist",
  400205:"One or more of the entered Chargeback codes are in the expected state",
  400206:"The owner of one or more of the entered chargeback codes is inactive.",
  400207:"One or more of the entered Chargeback codes is associated with an active Queue",
  400208:"One or more of the chargeback codes entered is frozen. Frozen chargeback codes cannot be processed",
  400209:"There are no active orgnizational approvals for the requester in system",
  400210:"The new owner is same as the present one. Transfer is not possible",
  400211:"The service has open incidents",
  400212:"One or more of the entered Chargeback codes is associated with an active Service",

  // Queue Error Codes 251 - 300
  400251:"No Queues available in the system",
  400252:"One or more of the entered Queues are active",
  400253:"One or more of the entered Queues are inactive",
  400254:"One or more of the entered Queues do not exist",
  400255:"One or more of the entered Queues are in the expected state",
  400256:"One or more of the entered users is already a member of the queue.",
  400257:"The Queue entered does not have any members to dissociate.",
  400258:"One or more of the entered users is not a member of the queue.",
  400259:"An active queue cannot be emptied of all its active members",
  400260:"There must be atleast one active member in the queue",
  400261:"The Chargeback code of one or more of the entered queues is inactive",
  400262:"One or more of the entered queues has an inactive owner or manager",
  400263:"The Approval request has already been processed",
  400264:"One or more of the queues entered is frozen. Frozen queues cannot be processed",
  400265:"One or more queues have open incidents",
  400266:"One or more members have open incidents for this queue",
  400267:"The queue has active services depending on it(CIs within APPROVED and RECLAIM)",

  // Configuration Item Type Error Codes 301 - 310
  400301:"No Configuration Item Types available in the system",
  400302:"One or more entered Configuration Item Types already exist",
  400303:"One or more entered Configuration Item Types do not exist",

  // Configuration Item environment Error codes 311 - 320
  400311:"No Configuration Item environments available in the system",
  400312:"One or more environments entered already exist in the system",
  400313:"The entered environments do not exist",

  // Configuration Item location Error codes 321 - 330
  400321:"No Configuration Item locations available in the system",
  400322:"The entered location does not exist",

  // Vendor Error Codes - 331 - 340
  400331:"No vendors available in the system",
  400332:"One or more of entered vendors do not exist",
  400333:"One or more vendors entered already exist in the system",

  // CINetwork Error Codes - 341 - 350
  400341:"No Configuration Item Network available in the system",
  400342:"The entered Configuration Item Network does not exist",
  400343:"The Configuration Item Network entered already exists in the system",
  400344:"The requested IP already exists",

  // CIHardware Error Codes - 351 - 360
  400351:"No Configuration Item Hardware available in the system",
  400352:"The entered Configuration Item Hardware does not exist",
  400353:"The Configuration Item Hardware entered already exists in the system",

  // Services Error Codes - 361 - 380
  400361:"No Services available in the system",
  400362:"One or more of the entered service do not exist",
  400363:"The requested service is frozen",
  400364:"The service is associated with one or more CIs",
  400365:"The service cannot be taken to an undefined state",
  400366:"The approval has already been processed",
  400367:"The user is not the designated approver",
  400368:"The approval ID doesn't exist",
  400369:"One or more services is not in approved state",
  400370:"Incorrect service",
  400371:"The service has pending approvals associated with it",
  400372:"No new service association requested",
  400373:"Configuration Item between Approved and Commissioned state must be associated with a service",
  400374:"Service cannot be cancelled as it's not in the registration state",
  400375:"Incorrect CI",
  400376:"Service state doesn't permit action. Please use '/orgapproval/initiatetransfer' to update chargeback code.",
  400377:"The requester has no pending service approvals",
  400378:"Service not in 'approved' state",
  400379:"One or more of the services being removed have open incidents for the CI",

  // CIDetails Error Codes - 381 - 400
  400381:"No Configuration Items available in the system",
  400382:"One or more of entered Configuration Items do not exist",
  400383:"Requested Configuration Item is frozen at the moment.",
  400384:"Configuration Item cannot be moved to undefined state",
  400385:"Configuration Item is associated with services",
  400386:"Configuration Item must be associted with atleast one service",
  400387:"Configuration Item state doesn't support the requested operation",
  400388:"Configuration Item doesn't have any support queue",
  400389:"Configuration Item is not associated with the service",
  400390:"Configuration Item has open incidents",
  400391:"Configuration Item has open incidents with the requested queue",

  // Attachment Error Codes - 401 - 410
  400401:"No file attached",
  400402:"Requested file doesn't exist",
  400403:"Maximum supported file size is 5MB",
  400405:"One or more of the attachments do not exist",
  400406:"Please provide unique set of attachments",

  // Incident Error Codes - 451 - 500
  400451:"No incidents in the system",
  400452:"Invalid severity code",
  400453:"Invalid impact code",
  400454:"One or more of the entered Incidents do not exist",
  400455:"The ticket is closed. Cannot be modified",
  400456:"The current state doesn't allow transition to the requested state",
  400457:"Invalid state transition request",
  400458:"Ticket needs an assignee to move to further states",
  400459:"Must provide a queueid to transisition into OPEN state",
  400460:"Ticket is already assigned to the requested queue",
  400461:"Please provide an explanation in the journal",
  400462:"Please provide valid res code, res notes and endedAt",
  400463:"Only journal entries are allowed while marking UNRESOLVED",
  400464:"Please provide the closure notes",
  400465:"Only closure notes are allowed while marking CLOSED",
  400466:"Please provide the incident number causing the pendency of this ticket",
  400467:"Please provide the vendor",
  400468:"The ticket is already assigned to the requested member",
  400469:"End time cannot be earlier then start time",
  400470:"Requester is not part of any of the queues",
  400471:"Requester doesn't have the privilege to perform the action",
  400472:"All the sent attachments are already attached",
  400473:"Current state of the incident doesn't allow cancelation",
  400474:"Requester doesn't belong to the owner group",
  400475:"Same assigned queue cannot be used to move the ticket to OPEN state",
  400476:"One or more of the attributes cannot be changed at this state",
  400477:"Invalid satisfaction rating",

  // Template Error Codes - 501 - 520
  400501:"The requested template doesn't exist",

  //Company Error Codes - 521 - 540
  400521:"Company doesn't exist",
  400522:"Account administrator must have the super user privilege",
  400523:"Account doesn't exist",

  // Slack Error Codes - 541 - 600
  400541: "Requested incident doesn't exist",
  400542: "Requested service doesn't exist",
  400543: "Requested ci doesn't exist",
  400544: "Requested user doesn't exist",

  // Loggly Error Codes - 601 - 650
  400601: "Provided credentials are incorrect",
  400602: "Provided token is incorrect",
};
