  /**
 * Created by gkatzioura on 6/20/16.
 */
 
//var getConnect = function (text,cb) {
// 
//  sails.log.info("Should send text: "+text);
//  cb();
//};

var request = require('request-promise');
var cache = require('memory-cache');
var promise = require('bluebird');

module.exports =  
{
  getKey: function()
  {
    //TODO: Retrieve a list of accounts to run weaved stuff on
    var authToken = cache.get("WEAVEDTESTACCOUNT" + ":" + "remot3AuthToken");
    if(authToken)
    {
      sails.log.info("Found key in cache", authToken);
      return authToken;
    }
    else
    {
      sails.log.info("No key in cache");
      //TODO: Retrieve the weaved username/password of this account
      var username = "parth.pani@queueload.com";
      var password = "weaved@1234";
      sails.log.info("[ Username: ", username, "]");
      sails.log.info("[ Password: ", password, "]");
      var options = 
      {
        uri: "https://api.weaved.com/v22/api/user/login/" + username + "/" + password,
        headers: {
          'User-Agent': 'Request-Promise',
          'content-type' : 'application/json',
          'apikey' : 'WeavedDemoKey\$2015'
        }
      };
      return request(options);
    }
  },
  getDevList: function(token)
  {
    sails.log.info("[ Token: ", token, "]");
    var options = 
    {
      uri: "https://api.weaved.com/v22/api/device/list/all",
      headers: {
        'User-Agent': 'Request-Promise',
        'content-type' : 'application/json',
        'apikey' : 'WeavedDemoKey\$2015',
        'token' : token
      }
    };
    return request(options);
  },
  getConnect: function(token, devAddrArray, ip)
  {
    sails.log.info("[ Token: ", token, "]");
    sails.log.info("[ Device Address: ", devAddrArray, "]");
    sails.log.info("[ Host IP: ", ip, "]");
    var fn = function(devaddress)
    {
      var options = 
      {
        method: 'POST',
        uri: "https://api.weaved.com/v22/api/device/connect",
        headers: {
          'User-Agent': 'Request-Promise',
          'content-type' : 'application/json',
          'apikey' : 'WeavedDemoKey\$2015',
          'token' : token,
        },
        body : {
          "deviceaddress": devaddress,
          "hostip": ip, 
          "wait": true
        },
        json: true
      };
      sails.log.info("Returning the request with devaddress", devaddress, "ip", ip, "authToken", token);
      return request(options);
    };
    var actions = devAddrArray.map(fn);
    var pResults = promise.all(actions).reflect();
    return pResults;
  }
};