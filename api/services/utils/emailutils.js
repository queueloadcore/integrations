/* FileName     : EmailUtils.js
 *
 * @description  : Generic utility functions.
 *                Unless specified all the functions return a promise object.
 *
 *   Date              Change Description                            Author
 * ---------         ----------------------                        ---------
 * 01/05/2017         Initial file creation                         SMandal
 *
 */

var cheerio = require('cheerio');

module.exports =
{
  sentrysplit: function(tempData)
  {
    sails.log.info("[EmailUtils] tempData", tempData);
  	var lines = tempData.text.split("\n");
    var data = [];
    var i, j, x;

    for (i = 0; i < lines.length; i++) {
        var line = lines[i];
        if (line.indexOf("https") >= 0 && line.indexOf("issues") >= 0) {
            //console.log("line1 : ", lines[i]);
            data[0] = line;
        } else if (line.indexOf("* level") >= 0) {
            //console.log("line 2: ", lines[i]);
            x = line.indexOf("=");
            data[1] = line.substring(x + 2);
        } else if (line.indexOf("* server_name") >= 0) {
            //console.log("line 3: ", lines[i]);
            x = line.indexOf("=");
            data[2] = line.substring(x + 2);
        } else if (line.indexOf("Exception") >= 0) {
            //console.log("line 4: ", lines[i]);
            for (j = i + 2; j < lines.length; j++) {
                if (lines[j] === "Message") break;
            }
            //console.log("the indexes are: ", i, j);
            data.push(lines.slice(i + 3, j - 2));
            break;
        } else if (line.indexOf("## Issue Details") >= 0) {
            //console.log("line 4: ", lines[i]);
            for (j = i + 2; j < lines.length; j++) {
                if (lines[j].indexOf("https") >= 0 && lines[j].indexOf("issues") >= 0) break;
            }
            //console.log("the indexes are: ", i, j);
            data[0] = lines[j];
            data.push(lines.slice(i + 2, j - 1));
            data.splice(2, 0, "");
            break;
        } else if (line.indexOf("# Regression") >= 0) {
            //console.log("line 2: ", lines[i]);
            data[1] = "Regression"
        }
    }
    
    $ = cheerio.load(tempData.html);
    var text = $('.event-date').text();
    if (text !== "") {
        data.splice(3, 0, text);
    } else {
        var text2 = $('.last-seen').text();
        x = text2.indexOf(":");
        data.splice(3, 0, text2.substring(x + 2));
    }

    return data;
  }
};