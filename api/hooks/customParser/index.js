/**
 * customEventParser hook
 */

 module.exports = function customeventparser(sails) {

/**
 * Module dependencies.
 */

  var RSMQWorker = require( "rsmq-worker" );
  var moment = require('moment');
  var promise = require('bluebird');
  var pgp = require('pg-promise')();
  var cache = require('memory-cache');
  var RedisMQ = require("rsmq");
  var rsmqOptions = { ns: "rsmq" };
  var enums = require("../../resources/Enums.js");
  var connection = {
    user: 'qlodtest',
    database: 'qlodtest',
    password: 'qlodtest'
  };
  if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'test')
  {
    rsmqOptions.host = sails.config.connections.localRefTknStore.host;
    rsmqOptions.port = sails.config.connections.localRefTknStore.port;
    connection.host = sails.config.connections.localAccountsRepo.host;
    connection.port = sails.config.connections.localAccountsRepo.port;
  }
  else if(process.env.NODE_ENV === 'production')
  {
    rsmqOptions.host = sails.config.connections.awsRefTknStore.host;
    rsmqOptions.port = sails.config.connections.awsRefTknStore.port;
    connection.host = sails.config.connections.awsDataRepo.host;
    connection.port = sails.config.connections.awsDataRepo.port;
  }
  else
  {
    sails.log.error("No environment set!!");
    process.exit(1);
  }

  var rsmq = new RedisMQ(rsmqOptions);
  promise.promisifyAll(rsmq);
  
  var messageDeleteWindow = 120000;
  var messageTooOld = 30000;
  
  var msgQName = "custom-events";
  var dlQName = "custom-events-deadletter";

  var exdCheckFn = function (msg)
  {
    if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
    {
      return false;
    }
    else if(msg.sent < (Math.floor(new Date) - messageTooOld))
    {
      rsmq.sendMessageAsync({qname: dlQName, message: msg.message})
      .then(function (id)
      {
        if(id)
        {
          return false;
        }
        else
        {
          return true;
        }
      })
      .catch(function ()
      {
        return true;
      });
    }
    else
    {
      return true;
    }
  };

  var options = 
  {
    timeout: 1,
    interval: [ 0.1, 0.3, 0.5, 1 ],
    invisibletime: 60 ,                    // hide received message for 60 sec
    maxReceiveCount: 1,                    // only receive a message once until delete
    autostart: true,                       // start worker on init
    customExceedCheck: exdCheckFn,
    rsmq: rsmq
  };

  /**
  * Expose hook definition
  */
  return {
    // Run when sails loads-- be sure and call `next()`.
    initialize: function (next) 
    {
      sails.after('hook:orm:loaded', function () 
      {
        var worker = new RSMQWorker( msgQName, options);
        worker.on( "data", function( msg )
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return exdCheckFn(msg);
          }
          else if(msg.sent < (Math.floor(new Date) - messageTooOld))
          {
            return exdCheckFn(msg);
          }
          else if(msg.rc > options.maxReceiveCount)
          {
            return;
          } 
          else
          {
            var convert = function(messageObj)
            {
              var eventObj = messageObj;
              eventObj.source = enums.integrationService.CUSTOM;
              eventObj.ci = messageObj.ci;
              eventObj.service = messageObj.service;
              eventObj.accountid = messageObj.qlaccountid;
              if(eventObj.type === enums.alarmType.INFO)
              {
                eventObj.state = enums.alarmState.OK;
              }
              else
              {
                eventObj.state = enums.alarmState.ALARM;
              }
              var db = {};
              db = cache.get(eventObj.qlaccountid);
              if(!db)
              {
                connection.database = eventObj.qlaccountid;
                connection.user = eventObj.qlaccountid;
                connection.password = eventObj.qlaccountid;
                db = pgp(connection);
                cache.put(connection.database, db);
              }
              var pDbQueries;
              var promiseArray = [rsmq.listQueuesAsync()];
              pDbQueries = db.tx(function (transaction)
              {
                var tasks = [];
                if(eventObj.ci && !eventObj.service)
                {
                  var sqlQuery = 'select servicename from services where serviceid=(select primaryservice from cidetails where ciname=\'' + eventObj.ci + '\');';
                  tasks.push(transaction.any(sqlQuery));
                }
                if(eventObj.ci)
                {
                  var sqlQuery = 'select locationname from cilocations where locationid=(select locationid from cidetails where ciname=\'' + eventObj.ci + '\');';
                  tasks.push(transaction.any(sqlQuery));
                }
                return transaction.batch(tasks);
              });
              promiseArray.push(pDbQueries);
              Promise.all(promiseArray)
              .then(function (results)
              {
                var queues = results[0];
                var seek = 0;
                if(eventObj.ci && !eventObj.service && results[1][seek].length)
                {
                  eventObj.service = results[1][seek][0].servicename;
                  seek++;
                }
                if(eventObj.ci && results[1][seek].length)
                {
                  eventObj.region = results[1][seek][0].locationname;
                  seek++;
                }
                if(queues.indexOf(messageObj.qlaccountid + "-alarms") < 0 || queues.indexOf(messageObj.qlaccountid + "-alarms-deadletter") < 0)
                {
                  var pArray = [];
                  if(queues.indexOf(messageObj.qlaccountid + "-alarms") < 0)
                  {
                    var pCreateAlarmQueue = rsmq.createQueueAsync({qname: messageObj.qlaccountid + "-alarms", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmQueue);
                  }
                  if(queues.indexOf(messageObj.qlaccountid + "-alarms-deadletter") < 0)
                  {
                    var pCreateAlarmDLQueue = rsmq.createQueueAsync({qname: messageObj.qlaccountid + "-alarms-deadletter", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmDLQueue);
                  }
                  return promise.all(pArray);
                }
                else
                {
                  return queues;
                }
              })
              .then(function (queue)
              {
                //sails.log.info("[hooks.customParser] Message going into the queue", eventObj);
                eventObj.rawdata = JSON.parse(msg.message).rawdata;
                return rsmq.sendMessageAsync({qname: JSON.parse(msg.message).qlaccountid + "-alarms", message: JSON.stringify(eventObj)});
              })
              .then(function (eventObj)
              {
                return rsmq.sendMessageAsync({qname: dlQName, message: msg.message});
              })
              .then(function (message)
              {
                return rsmq.deleteMessageAsync({qname: msgQName, id: msg.id});
              })
              .then(function (message)
              {
                return;
              })
              .catch(function (error)
              {
                sails.log.error("[hooks.customParser.onData] Error", error);
                return error;
              });
            };
            return convert(JSON.parse(msg.message));
          }
        });
        worker.on('error', function( err, msg ) {});
        worker.on('exceeded', function( msg ) {});
        worker.on('timeout', function( msg ){
          if(msg.rc > 0)
          {
            return exdCheckFn(msg);
          }
          else
          {
            return;
          }
        });
        
        worker.start();

        var dlExdCheckFn = function (msg)
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return false;
          }
          else
          {
            return true;
          }
        };
        
        var dlOptions = 
        {
          timeout: 1,
          interval: [ 0.1, 0.3, 0.5, 1 ],
          invisibletime: 60,                     // hide received message for 6 hours
          maxReceiveCount: 1,                    // only receive a message once until delete
          autostart: true,                       // start worker on init
          customExceedCheck: dlExdCheckFn,
          rsmq: rsmq
        };
        
        var dlWorker = new RSMQWorker( dlQName, dlOptions);
        
        dlWorker.on("message", function(dlmsg, dlnext, dlid)
        {
          dlnext({delete: false});
        });
        
        // optional error listeners
        dlWorker.on('error', function( err, dlmsg ) {});
        dlWorker.on('exceeded', function( dlmsg ) {});
        dlWorker.on('timeout', function( dlmsg ) {});
        
        dlWorker.start();
        return next();                
      });
    }
  }
};