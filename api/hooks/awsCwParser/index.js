/**
 * AwsCwEventParser hook
 */

module.exports = function awscweventparser(sails) {
/**
 * Module dependencies.
 */

  var RSMQWorker = require( "rsmq-worker" );
  var flatten = require('flat');
  var moment = require('moment');
  var promise = require('bluebird');
  var pgp = require('pg-promise')();
  var cache = require('memory-cache');
  var RedisMQ = require("rsmq");
  var rsmqOptions = { ns: "rsmq" };
  var enums = require("../../resources/Enums.js");
  var connection = {
    user: 'qlodtest',
    database: 'qlodtest',
    password: 'qlodtest'
  };
  if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'test')
  {
    rsmqOptions.host = sails.config.connections.localRefTknStore.host;
    rsmqOptions.port = sails.config.connections.localRefTknStore.port;
    connection.host = sails.config.connections.localAccountsRepo.host;
    connection.port = sails.config.connections.localAccountsRepo.port;
  }
  else if(process.env.NODE_ENV === 'production')
  {
    rsmqOptions.host = sails.config.connections.awsRefTknStore.host;
    rsmqOptions.port = sails.config.connections.awsRefTknStore.port;
    connection.host = sails.config.connections.awsDataRepo.host;
    connection.port = sails.config.connections.awsDataRepo.port;
  }
  else
  {
    sails.log.error("No environment set!!");
    process.exit(1);
  }

  var rsmq = new RedisMQ(rsmqOptions);
  promise.promisifyAll(rsmq);
  
  var messageDeleteWindow = 120000;
  var messageTooOld = 30000;
  
  var templateObj =
  {
    "ci" : "Trigger.Dimensions.0.value",
    "reportedAt" : "StateChangeTime",
    "metric" : "Trigger.MetricName",
    "component" : "Trigger.Namespace",
    "summary" : "NewStateReason",
    "state" : "NewStateValue",
    "region" : "Region",
    "name" : "AlarmName"
  };
  
  var msgQName = "awscw-events";
  var dlQName = "awscw-events-deadletter";
  
  var exdCheckFn = function (msg)
  {
    if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
    {
      return false;
    }
    else if(msg.sent < (Math.floor(new Date) - messageTooOld))
    {
      rsmq.sendMessageAsync({qname: dlQName, message: msg.message})
      .then(function (id)
      {
        if(id)
        {
          return false;
        }
        else
        {
          return true;
        }
      })
      .catch(function ()
      {
        return true;
      });
    }
    else
    {
      return true;
    }
  };
  
  var options = 
  {
    timeout: 1,
    interval: [ 0.1, 0.3, 0.5, 1 ],
    invisibletime: 60 ,                    // hide received message for 60 sec
    maxReceiveCount: 1,                    // only receive a message once until delete
    autostart: true,                       // start worker on init
    customExceedCheck: exdCheckFn,
    rsmq: rsmq
  };
  
    /**
     * Expose hook definition
     */

  return {
    // Run when sails loads-- be sure and call `next()`.
    initialize: function (next) 
    {
      sails.after('hook:orm:loaded', function () 
      {
        var worker = new RSMQWorker( msgQName, options);
        
        worker.on( "data", function( msg )
        {
          // process your message
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return exdCheckFn(msg);
          }
          else if(msg.sent < (Math.floor(new Date) - messageTooOld))
          {
            return exdCheckFn(msg);
          }
          else if(msg.rc > options.maxReceiveCount)
          {
            return;
          } 
          else
          {
            var messageObj = JSON.parse(msg.message);
            var decideEventType = function(object, convert)
            {
              if(convert['Trigger.MetricName'] === 'CPUUtilization')
              {
                var sum = convert['Trigger.Period'] * convert['Trigger.EvaluationPeriods'] * convert['Trigger.Threshold'];
                if(sum >= 600)
                {
                  object.type = enums.alarmType.ERROR;
                }
                return object;
              }
              else if(convert['Trigger.MetricName'] === 'NetworkOut')
              {
                var sum = convert['Trigger.Period'] * convert['Trigger.EvaluationPeriods'] * convert['Trigger.Threshold'];
                if(sum > 60000)
                {
                  object.type = enums.alarmType.ERROR;
                }
                return object;
              }
              else
              {
                return object;
              }
            };
          
            var convertFnc = function(template, convert)
            {
              var jsonifiedMessage = JSON.parse(msg.message);
              var eventObj = {type: enums.alarmType.INFO, accountid : jsonifiedMessage.qlaccountid, source: enums.integrationService.CLOUDWATCH};
              Object.keys(template).forEach(function (templateKey)
              {
                for (var ele in convert) {
                  if (ele === template[templateKey])
                  { 
                    eventObj[templateKey] = convert[ele];
                  }
                }
              });
              eventObj.reportedAt = moment(eventObj.reportedAt).unix();
              switch(eventObj.state)
              {
                case 'OK':
                  eventObj.state = enums.alarmState.OK;
                  break;
                case 'ALARM':
                  eventObj.state = enums.alarmState.ALARM;
                  break;
                default:
                  break;
              }
              var db = {};
              db = cache.get(jsonifiedMessage.qlaccountid);
              if(!db)
              {
                connection.database = jsonifiedMessage.qlaccountid;
                connection.user = jsonifiedMessage.qlaccountid;
                connection.password = jsonifiedMessage.qlaccountid;
                db = pgp(connection);
                cache.put(connection.database, db);
              }
              var sqlQuery = 'select servicename from services where serviceid=(select primaryservice from cidetails where ciname=\'' + eventObj.ci + '\');';
              db.query(sqlQuery)
              .then(function (result)
              {
                if(result.length)
                {
                  eventObj.service = result[0].servicename;
                }
                else
                {
                  eventObj.service = null;
                }
                if(convert.NewStateValue != 'OK' && convert['Trigger.Statistic'] === 'AVERAGE')
                {
                  return decideEventType(eventObj, convert);
                }
                else
                {
                  return eventObj;
                }
              })
              .then(function (eventObj)
              {
                return rsmq.listQueuesAsync();
              })
              .then(function (queues)
              {
                if(queues.indexOf(jsonifiedMessage.qlaccountid + "-alarms") < 0 || queues.indexOf(jsonifiedMessage.qlaccountid + "-alarms-deadletter") < 0)
                {
                  var pArray = [];
                  if(queues.indexOf(jsonifiedMessage.qlaccountid + "-alarms") < 0)
                  {
                    var pCreateAlarmQueue = rsmq.createQueueAsync({qname: jsonifiedMessage.qlaccountid + "-alarms", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmQueue);
                  }
                  if(queues.indexOf(jsonifiedMessage.qlaccountid + "-alarms-deadletter") < 0)
                  {
                    var pCreateAlarmDLQueue = rsmq.createQueueAsync({qname: jsonifiedMessage.qlaccountid + "-alarms-deadletter", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmDLQueue);
                  }
                  return promise.all(pArray);
                }
                else
                {
                  return queues;
                }          
              })
              .then(function (queue)
              {
                eventObj.rawdata = JSON.parse(msg.message).rawdata;
                return rsmq.sendMessageAsync({qname: JSON.parse(msg.message).qlaccountid + "-alarms", message: JSON.stringify(eventObj)});
              })
              .then(function (eventObj)
              {
                return rsmq.sendMessageAsync({qname: dlQName, message: msg.message});
              })
              .then(function (message)
              {
                return rsmq.deleteMessageAsync({qname: msgQName, id: msg.id});
              })
              .then(function (message)
              {
                return;
              })
              .catch(function (error)
              {
                sails.log.error("[hooks.awsCwParser.onData] Error", error);
                return error;
              });
            };
            return convertFnc(templateObj, flatten(messageObj));
          }
        });
      
        // optional error listeners
        worker.on('error', function( err, msg ) {});
        worker.on('exceeded', function( msg ){});
        worker.on('timeout', function( msg ){
          if(msg.rc > 0)
          {
            return exdCheckFn(msg);
          }
          else
          {
            return;
          }
        });
        
        worker.start();
        
        var dlExdCheckFn = function (msg)
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return false;
          }
          else
          {
            return true;
          }
        };
        
        var dlOptions = 
        {
          timeout: 1,
          interval: [ 0.1, 0.3, 0.5, 1 ],
          invisibletime: 60,                     // hide received message for 6 hours
          maxReceiveCount: 1,                    // only receive a message once until delete
          autostart: true,                       // start worker on init
          customExceedCheck: dlExdCheckFn,
          rsmq: rsmq
        };
        
        var dlWorker = new RSMQWorker( dlQName, dlOptions);
        
        dlWorker.on("message", function(dlmsg, dlnext, dlid)
        {
          dlnext({delete: false});
        });
        
        // optional error listeners
        dlWorker.on('error', function( err, dlmsg ) {});
        dlWorker.on('exceeded', function( dlmsg ) {});

        dlWorker.on('timeout', function( dlmsg ) {});
        dlWorker.start();
      });
      return next();
    }
  };
};
