/**
 * logglyParser hook
 */

module.exports = function logglyeventparser(sails)
{
/**
 * Module dependencies.
 */

  var RSMQWorker = require( "rsmq-worker" );
  var moment = require('moment');
  var promise = require('bluebird');
  var pgp = require('pg-promise')();
  var cache = require('memory-cache');
  var _ = require('lodash');
  var RedisMQ = require("rsmq");
  var rsmqOptions = { ns: "rsmq" };
  var enums = require("../../resources/Enums.js");
  var connection = {
    user: 'qlodtest',
    database: 'qlodtest',
    password: 'qlodtest'
  };
  if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'test')
  {
    rsmqOptions.host = sails.config.connections.localRefTknStore.host;
    rsmqOptions.port = sails.config.connections.localRefTknStore.port;
    connection.host = sails.config.connections.localAccountsRepo.host;
    connection.port = sails.config.connections.localAccountsRepo.port;
  }
  else if(process.env.NODE_ENV === 'production')
  {
    rsmqOptions.host = sails.config.connections.awsRefTknStore.host;
    rsmqOptions.port = sails.config.connections.awsRefTknStore.port;
    connection.host = sails.config.connections.awsDataRepo.host;
    connection.port = sails.config.connections.awsDataRepo.port;
  }
  else
  {
    sails.log.error("No environment set!!");
    process.exit(1);
  }
  var rsmq = new RedisMQ(rsmqOptions);
  promise.promisifyAll(rsmq);
  
  var messageDeleteWindow = 120000;
  var messageTooOld = 30000;
  
  var msgQName = "loggly-events";
  var dlQName = "loggly-events-deadletter";

  var exdCheckFn = function (msg)
  {
    if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
    {
      return false;
    }
    else if(msg.sent < (Math.floor(new Date) - messageTooOld))
    {
      rsmq.sendMessageAsync({qname: dlQName, message: msg.message})
      .then(function (id)
      {
        if(id)
        {
          return false;
        }
        else
        {
          return true;
        }
      })
      .catch(function ()
      {
        return true;
      });
    }
    else
    {
      return true;
    }
  };

  var options = 
  {
    timeout: 1,
    interval: [ 0.1, 0.3, 0.5, 1 ],
    invisibletime: 60 ,                    // hide received message for 60 sec
    maxReceiveCount: 1,                    // only receive a message once until delete
    autostart: true,                       // start worker on init
    customExceedCheck: exdCheckFn,
    rsmq: rsmq
  };

    /**
     * Expose hook definition
     */

  return {
    initialize: function(next)
    {
      sails.after('hook:orm:loaded', function () 
      {
        var worker = new RSMQWorker( msgQName, options);
        worker.on( "data", function( msg )
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return exdCheckFn(msg);
          }
          else if(msg.sent < (Math.floor(new Date) - messageTooOld))
          {
            return exdCheckFn(msg);
          }
          else if(msg.rc > options.maxReceiveCount)
          {
            return;
          } 
          else
          {
            var convert = function(messageObj)
            {
              var eventObj = {type: enums.alarmType.ERROR, accountid : messageObj.qlaccountid, source: enums.integrationService.LOGGLY};
              eventObj.name = "[ " + messageObj.alert_name + " ]";
              eventObj.qlaccountid = messageObj.qlaccountid;
              eventObj.ci = null;
              eventObj.reportedAt = moment(messageObj.start_time, "MMM D hh:mm:ss").unix();
              eventObj.metric = 'CODE';
              eventObj.component = null;
              eventObj.summary = JSON.stringify(messageObj.recent_hits);
              eventObj.region = null;
              var db = {};
              db = cache.get(messageObj.qlaccountid);
              if(!db)
              {
                connection.database = messageObj.qlaccountid;
                connection.user = messageObj.qlaccountid;
                connection.password = messageObj.qlaccountid;
                db = pgp(connection);
                cache.put(connection.database, db);
              }
              return db.any(
              {
                text: "select config->'servicetoalertmaps' AS maps from integrationconfigs WHERE integrationtype=$1;",
                values: [ enums.integrationService.LOGGLY ]
              })
              .then(function (configs)
              {
                var promiseArray = [rsmq.listQueuesAsync()];
                var map = _.filter(configs[0].maps, ['alertname', messageObj.alert_name ]);
                if(map.length)
                {
                  var serviceName = db.one(
                  {
                    text: "select servicename from services WHERE serviceid=$1;",
                    values: [ map[0].serviceid ]
                  });
                  promiseArray.push(serviceName);
                }
                return Promise.all(promiseArray);
              })
              .then(function (results)
              {
                var queues = results[0];
                if(results[1])
                {
                  eventObj.service = results[1].servicename; 
                }
                else
                {
                  eventObj.service = null;
                }
                if(queues.indexOf(messageObj.qlaccountid + "-alarms") < 0 || queues.indexOf(messageObj.qlaccountid + "-alarms-deadletter") < 0)
                {
                  var pArray = [];
                  if(queues.indexOf(messageObj.qlaccountid + "-alarms") < 0)
                  {
                    var pCreateAlarmQueue = rsmq.createQueueAsync({qname: messageObj.qlaccountid + "-alarms", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmQueue);
                  }
                  if(queues.indexOf(messageObj.qlaccountid + "-alarms-deadletter") < 0)
                  {
                    var pCreateAlarmDLQueue = rsmq.createQueueAsync({qname: messageObj.qlaccountid + "-alarms-deadletter", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmDLQueue);
                  }
                  return promise.all(pArray);
                }
                else
                {
                  return queues;
                }
              })
              .then(function (queue)
              { 
                eventObj.rawdata = JSON.parse(msg.message).rawdata;
                return rsmq.sendMessageAsync({qname: JSON.parse(msg.message).qlaccountid + "-alarms", message: JSON.stringify(eventObj)});
              })
              .then(function (eventObj)
              {
                return rsmq.sendMessageAsync({qname: dlQName, message: msg.message});
              })
              .then(function (message)
              {
                return rsmq.deleteMessageAsync({qname: msgQName, id: msg.id});
              })
              .then(function (message)
              {
                return;
              })
              .catch(function (error)
              {
                sails.log.error("[hooks.logglyParser.onData] Error", error);
                return error;
              });
            };
            return convert(JSON.parse(msg.message));
          }
        });
        worker.on('error', function( err, msg ) {});
        worker.on('exceeded', function( msg ) {});
        worker.on('timeout', function( msg ){
          if(msg.rc > 0)
          {
            return exdCheckFn(msg);
          }
          else
          {
            return;
          }
        });
        
        worker.start();

        var dlExdCheckFn = function (msg)
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return false;
          }
          else
          {
            return true;
          }
        };
        
        var dlOptions = 
        {
          timeout: 1,
          interval: [ 0.1, 0.3, 0.5, 1 ],
          invisibletime: 60,                     // hide received message for 6 hours
          maxReceiveCount: 1,                    // only receive a message once until delete
          autostart: true,                       // start worker on init
          customExceedCheck: dlExdCheckFn,
          rsmq: rsmq
        };
        
        var dlWorker = new RSMQWorker( dlQName, dlOptions);
        
        dlWorker.on("message", function(dlmsg, dlnext, dlid)
        {
          dlnext({delete: false});
        });
        
        // optional error listeners
        dlWorker.on('error', function( err, dlmsg ) {});
        dlWorker.on('exceeded', function( dlmsg ) {});
        dlWorker.on('timeout', function( dlmsg ) {});
        
        dlWorker.start();
        return next();
      });
    }
  };
};