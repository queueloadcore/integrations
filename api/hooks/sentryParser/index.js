/**
 * sentryParser hook
 */

module.exports = function sentryeventparser(sails)
{
/**
 * Module dependencies.
 */
  var RSMQWorker = require( "rsmq-worker" );
  var promise = require('bluebird');
  var pgp = require('pg-promise')();
  var cache = require('memory-cache');
  var _ = require('lodash');
  var RedisMQ = require("rsmq");
  var rsmqOptions = { ns: "rsmq" };
  var enums = require("../../resources/Enums.js");
  var connection = {
    user: 'qlodtest',
    database: 'qlodtest',
    password: 'qlodtest'
  };
  if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'test')
  {
    rsmqOptions.host = sails.config.connections.localRefTknStore.host;
    rsmqOptions.port = sails.config.connections.localRefTknStore.port;
    connection.host = sails.config.connections.localAccountsRepo.host;
    connection.port = sails.config.connections.localAccountsRepo.port;
  }
  else if(process.env.NODE_ENV === 'production')
  {
    rsmqOptions.host = sails.config.connections.awsRefTknStore.host;
    rsmqOptions.port = sails.config.connections.awsRefTknStore.port;
    connection.host = sails.config.connections.awsDataRepo.host;
    connection.port = sails.config.connections.awsDataRepo.port;
  }
  else
  {
    sails.log.error("No environment set!!");
    process.exit(1);
  }
  var rsmq = new RedisMQ(rsmqOptions);
  promise.promisifyAll(rsmq);
  
  var messageDeleteWindow = 120000;
  var messageTooOld = 30000;
  
  var msgQName = "sentry-events";
  var dlQName = "sentry-events-deadletter";

  var exdCheckFn = function (msg)
  {
    if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
    {
      return false;
    }
    else if(msg.sent < (Math.floor(new Date) - messageTooOld))
    {
      rsmq.sendMessageAsync({qname: dlQName, message: msg.message})
      .then(function (id)
      {
        if(id)
        {
          return false;
        }
        else
        {
          return true;
        }
      })
      .catch(function ()
      {
        return true;
      });
    }
    else
    {
      return true;
    }
  };

  var options = 
  {
    timeout: 1,
    interval: [ 0.1, 0.3, 0.5, 1 ],
    invisibletime: 60 ,                    // hide received message for 60 sec
    maxReceiveCount: 1,                    // only receive a message once until delete
    autostart: true,                       // start worker on init
    customExceedCheck: exdCheckFn,
    rsmq: rsmq
  };

    /**
     * Expose hook definition
     */

  return {
    initialize: function(next)
    {
      sails.after('hook:orm:loaded', function () 
      {
        var worker = new RSMQWorker( msgQName, options);
        worker.on( "data", function( msg )
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return exdCheckFn(msg);
          }
          else if(msg.sent < (Math.floor(new Date) - messageTooOld))
          {
            return exdCheckFn(msg);
          }
          else if(msg.rc > options.maxReceiveCount)
          {
            return;
          } 
          else
          {
            var convert = function(messageObj)
            {
              var eventObj = {type: enums.alarmType.INFO, accountid : messageObj.qlaccountid, source: enums.integrationService.SENTRY};
              eventObj.name = "[ " + messageObj.project_name + " ] " + messageObj.message;
              eventObj.qlaccountid = messageObj.qlaccountid;
              eventObj.service = messageObj.project;
              if(messageObj.event.tags.length)
              {
                messageObj.event.tags.every(function(tag, ele)
                {
                  if(tag[0] === 'server_name')
                  {
                    eventObj.ci = tag[1];
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                });
              }              
              eventObj.reportedAt = messageObj.event.received;
              if(messageObj.level === 'error')
              {
                eventObj.type = enums.alarmType.ERROR;
              }
              eventObj.metric = 'CODE';
              eventObj.component = null;
              eventObj.summary = messageObj.event['sentry.interfaces.Exception'] ? JSON.stringify(messageObj.event['sentry.interfaces.Exception']) : JSON.stringify(messageObj.event['sentry.interfaces.Message']);
              switch(eventObj.type)
              {
                case enums.alarmType.ERROR:
                  eventObj.state = enums.alarmState.ALARM;
                  break;
                case enums.alarmType.INFO:
                  eventObj.state = enums.alarmState.INFO;
                  break;
                default:
                  break;
              }
              var db = {};
              db = cache.get(messageObj.qlaccountid);
              if(!db)
              {
                connection.database = messageObj.qlaccountid;
                connection.user = messageObj.qlaccountid;
                connection.password = messageObj.qlaccountid;
                db = pgp(connection);
                cache.put(connection.database, db);
              }
              var pDbQueries;
              var promiseArray = [rsmq.listQueuesAsync()];
              if(eventObj.ci)
              {
                pDbQueries = db.tx(function (transaction)
                {
                  var sqlQuery1 = 'select servicename from services where serviceid=(select primaryservice from cidetails where ciname=\'' + eventObj.ci + '\');';
                  var sqlQuery2 = 'select locationname from cilocations where locationid=(select locationid from cidetails where ciname=\'' + eventObj.ci + '\');';
                  var task1 = transaction.any(sqlQuery1);
                  var task2 = transaction.any(sqlQuery2);
                  return transaction.batch([task1, task2]);
                });
                promiseArray.push(pDbQueries);
              }
              Promise.all(promiseArray)
              .then(function (results)
              {
                var queues = results[0];
                if(eventObj.ci)
                {
                  if(results[1][0].length)
                  {
                    eventObj.service = results[1][0][0].servicename;
                  }
                  if(results[1][1].length)
                  {
                    eventObj.region = results[1][1][0].locationname;  
                  }
                }
                if(queues.indexOf(messageObj.qlaccountid + "-alarms") < 0 || queues.indexOf(messageObj.qlaccountid + "-alarms-deadletter") < 0)
                {
                  var pArray = [];
                  if(queues.indexOf(messageObj.qlaccountid + "-alarms") < 0)
                  {
                    var pCreateAlarmQueue = rsmq.createQueueAsync({qname: messageObj.qlaccountid + "-alarms", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmQueue);
                  }
                  if(queues.indexOf(messageObj.qlaccountid + "-alarms-deadletter") < 0)
                  {
                    var pCreateAlarmDLQueue = rsmq.createQueueAsync({qname: messageObj.qlaccountid + "-alarms-deadletter", vt: 60, maxsize: -1});
                    pArray.push(pCreateAlarmDLQueue);
                  }
                  return promise.all(pArray);
                }
                else
                {
                  return queues;
                }
              })
              .then(function (queue)
              { 
                eventObj.rawdata = JSON.parse(msg.message).rawdata;
                return rsmq.sendMessageAsync({qname: JSON.parse(msg.message).qlaccountid + "-alarms", message: JSON.stringify(eventObj)});
              })
              .then(function (eventObj)
              {
                return rsmq.sendMessageAsync({qname: dlQName, message: msg.message});
              })
              .then(function (message)
              {
                return rsmq.deleteMessageAsync({qname: msgQName, id: msg.id});
              })
              .then(function (message)
              {
                return;
              })
              .catch(function (error)
              {
                sails.log.error("[hooks.sentryParser.onData] Error", error);
                return error;
              });
            };
            return convert(JSON.parse(msg.message));
          }
        });

        // optional error listeners
        worker.on('error', function( err, msg ) {});
        worker.on('exceeded', function( msg ){});
        worker.on('timeout', function( msg ){
          if(msg.rc > 0)
          {
            return exdCheckFn(msg);
          }
          else
          {
            return;
          }
        });
        
        worker.start();

        var dlExdCheckFn = function (msg)
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return false;
          }
          else
          {
            return true;
          }
        };
        
        var dlOptions = 
        {
          timeout: 1,
          interval: [ 0.1, 0.3, 0.5, 1 ],
          invisibletime: 60,                     // hide received message for 6 hours
          maxReceiveCount: 1,                    // only receive a message once until delete
          autostart: true,                       // start worker on init
          customExceedCheck: dlExdCheckFn,
          rsmq: rsmq
        };
        
        var dlWorker = new RSMQWorker( dlQName, dlOptions);
        
        dlWorker.on("message", function(dlmsg, dlnext, dlid)
        {
          dlnext({delete: false});
        });
        
        // optional error listeners
        dlWorker.on('error', function( err, dlmsg ) {});
        dlWorker.on('exceeded', function( dlmsg ) {});
        dlWorker.on('timeout', function( dlmsg ) {});
        
        dlWorker.start();
        return next();
      });
    }
  };
};