/*
* FileName: negotiate.js
* @description: This file is the Generic Error Handler. Calls the appropriate custom response for a given error
*
* Date                        Change Description                               Author
* ---------               ---------------------------                        -----------
* 27/02/2016                Initial file creation                             Smruti Mandal
*/

var  _ = require("lodash");

module.exports = function(error)
{
  var res = this.res;

  var message = _.get(error, "reason") || _.get(error, "message");
  var root = _.get(error, "root");
  var data = _.get(error, "invalidAttributes") || _.omit(error, [ "name", "code", "reason", "message", "root", "status", "oauthError" ]);
  var statusCode = _.get(error, "status") || _.get(error, "oauthError") || 500;
  var code = _.get(error, "code") || statusCode;
  var stack = error.stack;
  var config = { code: code, message: message, root: root, stack: stack };

  if (code === 401 || (code >= 401501 && code <= 401600)) return res.unauthorized(data, config);
  else if (code === 403 || (code >= 403501 && code <= 403600)) return res.forbidden(data, config);
  else if (code === 4004) return res.notFound(data, config);
  else if (code >= 400001 && code < 400600) return res.badRequest(data, config);
  else if (code === 500 || code === 500001) return res.serverError(data, config);
  else return res.serverError(data, config);
};
