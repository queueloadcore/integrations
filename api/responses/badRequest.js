/**
 * 400 (Bad Request) Handler
 *
 * The server cannot or will not process the request due to something that is perceived to be a
 * client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).
 *
 *
 * Usage:
 * return res.badRequest();
 * return res.badRequest(data);
 * return res.badRequest(data, 'some/specific/badRequest/view');
 *
 * e.g.:
 * ```
 * return res.badRequest(
 *   'Please choose a valid `password` (6-12 characters)',
 *   'trial/signup'
 * );
 * ```
 */

var  _ = require("lodash");

module.exports = function badRequest(data, config)
{

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(400);

  var response = _.assign(
    {
      code: _.get(config, "code", "E_BAD_REQUEST"),
      message: _.get(config, "message", "The request cannot be fulfilled due to bad syntax"),
      data: data || {}
    }, _.get(config, "root", {}));

  // Log error to console
  sails.log.error('Sending 400 ("Bad Request") response: \n', response, config.stack);

  // Only include errors in response if application environment
  // is not set to 'production'.  In production, we shouldn't
  // send back any identifying information about errors.
  //TBD: Need to confirm this
  if (sails.config.environment === "production")
  {
    var code = response.code;
    response = {};
    response.code = code;
  }

  return res.jsonx(response);

};
