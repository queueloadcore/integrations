/**
 * 401 (Unauthorized) Response
 * Similar to 403 Forbidden.
 * Specifically for authentication failed or not yet provided.
 */
module.exports = function(data, code, message, root) {
  var response = _.assign({
    code: code.code || "E_UNAUTHORIZED",
    message: code.message || "Unauthorized user.",
    data: data || {}
  }, root);

  this.req._sails.log.error("Sent (401 UNAUTHORIZED)\n", response);

  this.res.status(401);
  if (sails.config.environment === "production")
  {
    var code = response.code;
    response = {};
    response.code = code;
  }
  this.res.jsonx(response);
};
