# Introduction


[QueueLoad](https://bitbucket.org/29bones/queueload-0.1) is an attempt to reduce alert fatigue and help organizations run silent operations.

Integrations is a nodejs application server which interacts with external sources such as CloudWatch, Nagios, Loggly etc to collate and process for each individual customer. This server is also responsible for processing requests coming from your Slack Channel.

The application is tested on Ubuntu 16.04 LTS.

The application can be run as a standalone.

# Requirements 

1. Linux Server
2. Account details of plugged in sources
3. Slack (Optional)

# Questions and issues

Feel free to raise issues with the tracker for any questions and issues. I may be a bit slow on response and I hope you understand that.

# License
The MIT License (MIT)

Copyright (c) 2016-2018 QueueLoad

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.