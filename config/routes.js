/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/' : {response: 'notFound'},
  // AAA
  'GET /auth/slackauth' : {controller: 'auth', action: 'slackauth', skipAssets: true},
  'POST /auth/slackevents' : {controller: 'auth', action: 'slackevents', skipAssets: true},
  // Slashcommands
  'POST /slashcommands/getuser' : {controller: 'slashcommands', action: 'getuser', skipAssets: true},
  'POST /slashcommands/getinc' : {controller: 'slashcommands', action: 'getinc', skipAssets: true},
  'POST /slashcommands/getservice' : {controller: 'slashcommands', action: 'getservice', skipAssets: true},
  'POST /slashcommands/getcidetails' : {controller: 'slashcommands', action: 'getcidetails', skipAssets: true},
  // Events
  'POST /event/receivecwevents' : {controller: 'event', action: 'receivecwevents', skipAssets: true},
  'POST /event/receivesentryevents' : {controller: 'event', action: 'receivesentryevents', skipAssets: true},
  'POST /event/receivenagiosevents' : {controller: 'event', action: 'receivenagiosevents', skipAssets: true},
  'POST /event/receivelogglyevents' : {controller: 'event', action: 'receivelogglyevents', skipAssets: true},
  'POST /event/receivecustomevents' : {controller: 'event', action: 'receivecustomevents', skipAssets: true},
  'POST /event/receiveazureevents' : {controller: 'event', action: 'receiveazureevents', skipAssets: true},
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/
};
